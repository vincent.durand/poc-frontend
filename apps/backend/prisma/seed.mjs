import { PrismaClient } from '@prisma/client'
import { randFullName, randBrand, randHex } from '@ngneat/falso';


// model Consultant {
//   id    Int     @id @default(autoincrement())
//   name  String?
//   assignments Assignment[]
// }


// model Assignment {
//   id    Int     @id @default(autoincrement())
//   consultant    Consultant?    @relation(fields: [consultantId], references: [id])
//   consultantId  Int?
//   project    Project?    @relation(fields: [projectId], references: [id])
//   projectId  Int?
//   from  DateTime
//   to DateTime
// } 


// model Project {
//   id    Int     @id @default(autoincrement())
//   name  String?
//   assignments Assignment[]
// } 

const prisma = new PrismaClient()

const CONSULTANTS = 100
const PROJECT = 10


async function main() {
  console.log(`Start seeding ...`)

  console.log(`SEED PROJECTS`)
  for (let i = 0; i < PROJECT; i++) {
     await prisma.project.create({
      data: {
        name: randBrand(),
        color: randHex()
      },
    })
  }
  console.log(`SEED CONSULTANTS`)
  for (let i = 0; i < CONSULTANTS; i++) {
    await prisma.consultant.create({
      data: {
        name: randFullName(),
      },
    })
  }

  console.log(`SEED ASSIGNMENTS`)

  // seed one assignement per week for each consultant
  // begin from 6 weeks ago and end 6 weeks from now
  const projects = await prisma.project.findMany()
  const consultants = await prisma.consultant.findMany()

  const now = new Date()
  const sixWeeksAgo = new Date()
  sixWeeksAgo.setDate(now.getDate() - 42)
  const sixWeeksFromNow = new Date()
  sixWeeksFromNow.setDate(now.getDate() + 42)

  const weeks = []
  for (let i = sixWeeksAgo; i <= sixWeeksFromNow; i.setDate(i.getDate() + 7)) {
    weeks.push(new Date(i))
  }

  for (const consultant of consultants) {
    for (const week of weeks) {
      const project = projects[Math.floor(Math.random() * projects.length)]
      const from = new Date(week)
      const to = new Date(week)
      to.setDate(to.getDate() + 4)
      await prisma.assignment.create({
        data: {
          consultant: {
            connect: {
              id: consultant.id,
            },
          },
          project: {
            connect: {
              id: project.id,
            },
          },
          from,
          to,
        },
      })
    }
  }



  console.log(`Seeding finished.`)
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })
