const express = require('express')
const { PrismaClient } = require('@prisma/client')
const cors = require('cors');

const prisma = new PrismaClient()
const app = express()

app.use(express.json())
app.use(cors());

// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', '*');
//   next();
// });


app.get('/consultants', async (req, res) => {
  const result = await prisma.consultant.findMany()
  res.json(result)
})

app.get('/consultants/:id/assignments', async (req, res) => {
  const { id } = req.params
  const result = await prisma.consultant
    .findUnique({
      where: {
        id: Number(id),
      },
    })
    .assignments({
      include: { project: true },
    })
  res.json(result)
})


// get all assignments
app.get('/assignments', async (req, res) => {
  const result = await prisma.assignment.findMany()
  res.json(result)
})

//get assignment
app.get(`/assignments/:id`, async (req, res) => {
  const { id } = req.params
  const assignment = await prisma.assignment.findUnique({
    where: {
      id: Number(id),
    },
  })
  res.json(assignment)
})

// get consultants assignments for given consultants id in body of post
app.post(`/consultants_assignments`, async (req, res) => {
  const { consultantsIds } = req.body
  const assignments = await prisma.assignment.findMany({
    where: {
      consultantId: {
        in: consultantsIds,
      },
    },
    include: { project: true },
  })

  await new Promise(resolve => setTimeout(resolve, 3000));

  res.json(assignments)
})



//delete assignment
app.delete(`/assignments/:id`, async (req, res) => {
  const { id } = req.params
  const assignment = await prisma.assignment.delete({
    where: {
      id: Number(id),
    },
  })
  res.json(assignment)
})

const server = app.listen(5005, () =>
  console.log(`
🚀 Server ready at: http://localhost:5005
⭐️ See sample requests: http://pris.ly/e/js/rest-express#3-using-the-rest-api`),
)
