"use server"

import {deleteAssignment as deleteAssignmentEntry} from 'api'
import { revalidatePath } from 'next/cache'

export default async function deleteAssignment(assignmentId: number) {
    await deleteAssignmentEntry(assignmentId)
    revalidatePath("/")
}