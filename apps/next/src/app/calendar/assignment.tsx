"use client";

import { Project, type Assignment } from "api";
import deleteAssignment from "../actions/deleteAssignment";
import { useOptimistic } from "react";

export const AssignmentButton = ({
  assignment,
}: {
  assignment: Assignment & { project: Project };
}) => {
  const [optimisticAssignment, removeOptimisticAssignment] = useOptimistic(
    assignment,
    () => null
  );

  return (
    optimisticAssignment && (
      <td key={optimisticAssignment.id}>
        <button
          onClick={() => {
            removeOptimisticAssignment(assignment.id);
            deleteAssignment(assignment.id);
          }}
          style={{
            appearance: "none",
            padding: 0,
            border: "none",
            width: "100%",
            display: "block",
            background: assignment.project.color,
            borderRadius: "20px",
          }}
        >
          <span
            style={{
              padding: "10px",
              background: "rgba(255,255,255,0.3)",
              borderRadius: "20px",
              display: "block",
              textAlign: "center",
            }}
          >
            {assignment.project.name}
          </span>
        </button>
      </td>
    )
  );
};
