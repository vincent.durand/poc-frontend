import {
  AssignmentWithProject,
  getConsultantAssignments,
  getGlobalCalendar,
} from "api";
import { AssignmentButton } from "./assignment";

export default async function Assignments({
  consultantId,
  consultantsAssignments,
}: {
  consultantId: number;
  consultantsAssignments: Promise<AssignmentWithProject[]>;
}) {
  const assignments =
    (await consultantsAssignments)?.filter(
      (assignment) => assignment.consultantId === consultantId
    ) ?? [];

  return assignments.map((assignment) => {
    return <AssignmentButton assignment={assignment} />;
  });
}
