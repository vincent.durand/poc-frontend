import {
  getConsultants,
  getConsultantsAssignments,
  getGlobalCalendar,
} from "api";
import { Suspense } from "react";
import Assignments from "./assignments";

export default async function Calendar() {
  const consultants = await getConsultants();

  const consultantsAssignments = getConsultantsAssignments(
    consultants.map((consultant) => consultant.id)
  );

  return (
    <div>
      <table>
        <tbody>
          {consultants.map((consultant) => {
            return (
              <tr key={consultant.id}>
                <td>
                  <strong>{consultant.name}</strong>
                </td>
                <Suspense fallback={<td>loading...</td>}>
                  <Assignments
                    consultantId={consultant.id}
                    consultantsAssignments={consultantsAssignments}
                  />
                </Suspense>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
