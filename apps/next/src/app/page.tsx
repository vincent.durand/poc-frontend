import Link from "next/link";

export default async function Home() {
  return (
    <div>
      <h1>HOMEPAGE</h1>
      <ul>
        <li>
          <Link href="/calendar">Calendar</Link>
        </li>
      </ul>
    </div>
  );
}
