import { Route as rootRoute } from './routes/__root'
import { Route as CalendarImport } from './routes/calendar/calendar'
import { Route as IndexImport } from './routes/index'

const CalendarRoute = CalendarImport.update({
  path: '/calendar',
  getParentRoute: () => rootRoute,
} as any)

const IndexRoute = IndexImport.update({
  path: '/',
  getParentRoute: () => rootRoute,
} as any)


declare module '@tanstack/react-router' {
  interface FileRoutesByPath {
    '/': {
      preLoaderRoute: typeof IndexImport
      parentRoute: typeof rootRoute
    }
    '/calendar': {
      preLoaderRoute: typeof CalendarImport
      parentRoute: typeof rootRoute
    }
  }
}
export const routeTree = rootRoute.addChildren([IndexRoute, CalendarRoute])
