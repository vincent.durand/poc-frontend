import { Project, type Assignment } from "api";
import { useDeleteAssignementMutation } from "./hooks/useDeleteAssignementMutation";
export const AssignmentButton = ({
  assignment,
}: {
  assignment: Assignment & { project: Project };
}) => {
  const mutation = useDeleteAssignementMutation(assignment.id);
  return (
    assignment && (
      <td key={assignment.id}>
        <button
          onClick={() => {
            mutation.mutate();
          }}
          style={{
            appearance: "none",
            padding: 0,
            border: "none",
            width: "100%",
            display: "block",
            background: assignment.project.color,
            borderRadius: "20px",
          }}
        >
          <span
            style={{
              padding: "10px",
              background: "rgba(255,255,255,0.3)",
              borderRadius: "20px",
              display: "block",
              textAlign: "center",
            }}
          >
            {assignment.project.name}
          </span>
        </button>
      </td>
    )
  );
};
