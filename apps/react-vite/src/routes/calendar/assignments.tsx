import { AssignmentWithProject } from "api";
import { AssignmentButton } from "./assignment";

export default function Assignments({
  consultantId,
  consultantsAssignments,
}: {
  consultantId: number;
  consultantsAssignments: AssignmentWithProject[];
}) {
  const assignments =
    consultantsAssignments?.filter(
      (assignment) => assignment.consultantId === consultantId
    ) ?? [];

  return assignments.map((assignment) => {
    return <AssignmentButton key={assignment.id} assignment={assignment} />;
  });
}
