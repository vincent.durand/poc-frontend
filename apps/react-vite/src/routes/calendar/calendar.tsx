import { FileRoute } from "@tanstack/react-router";
import Assignments from "./assignments";
import { useGetConsultantsAssignmentsQuery } from "./hooks/useGetConsultantsAssignmentsQuery";
import { useGetConsultantsQuery } from "./hooks/useGetConsultantsQuery";

export const Route = new FileRoute("/calendar").createRoute({
  component: CalendarComponent,
});

function CalendarComponent() {
  const { data: consultants } = useGetConsultantsQuery();
  const consultantsIds = consultants?.map((consultant) => consultant.id) ?? [];

  const { data: assignements, isLoading: isLoadingAssigments } =
    useGetConsultantsAssignmentsQuery(consultantsIds);

  return (
    <div>
      <table>
        <tbody>
          {consultants ? (
            consultants.map((consultant) => {
              return (
                <tr key={consultant.id}>
                  <td>
                    <strong>{consultant.name}</strong>
                  </td>
                  {isLoadingAssigments ? (
                    <td>Loading...</td>
                  ) : (
                    <Assignments
                      consultantId={consultant.id}
                      consultantsAssignments={assignements ?? []}
                    />
                  )}
                </tr>
              );
            })
          ) : (
            <tr>
              <td>Loading...</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
}
