import { useMutation, useQueryClient } from '@tanstack/react-query'
import { AssignmentWithProject, deleteAssignment } from 'api'
import { ASSIGNMENTS_KEY } from './useGetConsultantsAssignmentsQuery'

export const useDeleteAssignementMutation = (assignmentId: number) => {
    const queryClient = useQueryClient()

    return useMutation({
        mutationFn: () => deleteAssignment(assignmentId),
        onMutate: async () => {
          // Cancel any outgoing refetches
          // (so they don't overwrite our optimistic update)
          await queryClient.cancelQueries({ queryKey: [ASSIGNMENTS_KEY] })
      
          // Snapshot the previous value
          const previousTodos = queryClient.getQueryData([ASSIGNMENTS_KEY])
      
          // Optimistically update to the new value
          queryClient.setQueryData([ASSIGNMENTS_KEY], (oldAssignements: AssignmentWithProject[]) => oldAssignements.filter(assignment => assignment.id !== assignmentId) )
      
          // Return a context object with the snapshotted value
          return { previousTodos }
        },
        onSuccess: () => {
          // Invalidate and refetch
          queryClient.invalidateQueries({ queryKey: [ASSIGNMENTS_KEY] })
        },
      })
}