import { useQuery } from '@tanstack/react-query'
import { getConsultantsAssignments } from 'api'

export const ASSIGNMENTS_KEY = 'assignements'

export const useGetConsultantsAssignmentsQuery = (consultantsIds: number[]) => {
 return useQuery({ queryKey: [ASSIGNMENTS_KEY], queryFn: () => getConsultantsAssignments(consultantsIds), enabled: !!consultantsIds.length})
}