import { useQuery } from '@tanstack/react-query'
import { getConsultants } from 'api'

export const CONSULTANTS_KEY = 'consultants'

export const useGetConsultantsQuery = () => {
 return useQuery({ queryKey: [CONSULTANTS_KEY], queryFn: getConsultants })
}