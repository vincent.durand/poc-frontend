import {
  json,
  type LoaderFunctionArgs,
  type MetaFunction,
} from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";
import { getConsultants } from "api";
import { Suspense } from "react";

export const meta: MetaFunction = () => {
  return [{ title: "Home" }];
};

export async function loader({ params }: LoaderFunctionArgs) {
  const consultants = await getConsultants();

  return json({ consultants });
}

export default function Index() {
  const { consultants } = useLoaderData<typeof loader>();

  return (
    <div>
      <h1>HOMEPAGE</h1>
      <ul>
        <li>
          <Link to="/calendar">Calendar</Link>
        </li>
      </ul>
    </div>
  );
}
