"use client";

import { useFetcher } from "@remix-run/react";
import { Project, type Assignment } from "api";

export const AssignmentButton = ({
  assignment,
}: {
  assignment: Assignment & { project: Project };
}) => {
  const fetcher = useFetcher();
  const id = fetcher.formData?.get("assignmentId");
  const optimisticAssignment = assignment.id === Number(id) ? null : assignment;
  return (
    optimisticAssignment && (
      <td key={assignment.id}>
        <fetcher.Form method="post">
          <button
            type="submit"
            name="assignmentId"
            value={assignment.id}
            style={{
              appearance: "none",
              padding: 0,
              border: "none",
              width: "100%",
              display: "block",
              background: assignment.project.color,
              borderRadius: "20px",
            }}
          >
            <span
              style={{
                padding: "10px",
                background: "rgba(255,255,255,0.3)",
                borderRadius: "20px",
                display: "block",
                textAlign: "center",
              }}
            >
              {assignment.project.name}
            </span>
          </button>
        </fetcher.Form>
      </td>
    )
  );
};
