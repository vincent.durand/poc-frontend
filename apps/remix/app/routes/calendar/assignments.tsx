import { AssignmentWithProject } from "api";
import { AssignmentButton } from "./assignment";

export default function Assignments({
  assignments,
}: {
  assignments: AssignmentWithProject[];
}) {
  return assignments.map((assignment) => {
    return <AssignmentButton assignment={assignment} />;
  });
}
