import { Await, useLoaderData } from "@remix-run/react";
import { Suspense } from "react";
import Assignments from "./assignments";
import { type CalendarLoaderData } from "./route";

export function Calendar() {
  const { consultants, consultantsAssignments } =
    useLoaderData<CalendarLoaderData>();

  return (
    <div>
      <table>
        <tbody>
          {consultants.map((consultant) => {
            return (
              <tr key={consultant.id}>
                <td>
                  <strong>{consultant.name}</strong>
                </td>
                <Suspense fallback={<td>Loading ...</td>}>
                  <Await resolve={consultantsAssignments}>
                    {(assignments) => (
                      <Assignments
                        assignments={assignments.filter(
                          (assignment) =>
                            assignment.consultantId === consultant.id
                        )}
                      />
                    )}
                  </Await>
                </Suspense>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
