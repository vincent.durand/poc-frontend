import {
  ActionFunctionArgs,
  defer,
  type LoaderFunctionArgs,
  type MetaFunction,
} from "@remix-run/node";
import {
  deleteAssignment,
  getConsultants,
  getConsultantsAssignments,
} from "api";
import { Calendar } from "./calendar";

export const meta: MetaFunction = () => {
  return [{ title: "Calendar" }];
};

export async function loader({ params }: LoaderFunctionArgs) {
  const consultants = await getConsultants();

  const consultantsAssignments = getConsultantsAssignments(
    consultants.map((consultant) => consultant.id)
  );

  return defer({ consultants, consultantsAssignments });
}

export type CalendarLoaderData = typeof loader;

export const action = async ({ params, request }: ActionFunctionArgs) => {
  const formData = await request.formData();
  const assignmentId = formData.get("assignmentId");

  return await deleteAssignment(Number(assignmentId));
};

export default function Index() {
  return (
    <div>
      <Calendar />
    </div>
  );
}
