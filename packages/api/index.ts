const BACKEND = 'http://localhost:5005/'
const getUrl = (path: string) => `${BACKEND}${path}`

export type Consultant = {
    id: number
    name: string
}
export type Assignment = {
    id: number
    name: string
    consultantId: number
}

export type AssignmentWithProject = Assignment & { project: Project }

export type Project = {
    id: number
    name: string
    color: string
}

export type GlobalCalendarResponse = (Consultant & { assignments: (Assignment & {project : Project})[] })[]


export const getGlobalCalendar = async () => {
    const response =  await fetch(getUrl('global_calendar'))
    return await response.json() as GlobalCalendarResponse
}

export const getConsultants = async () => {
    const response =  await fetch(getUrl('consultants'))
    return await response.json() as Consultant[]
}

export const getConsultantAssignments = async (consultantId: number) => {
    const response =  await fetch(getUrl(`consultants/${consultantId}/assignments`))


    return await response.json() as AssignmentWithProject[]
}

export const getConsultantsAssignments = async (consultantsIds: number[]) => {
    const response =  await fetch(getUrl(`consultants_assignments`), {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({consultantsIds})
    })


    return await response.json() as AssignmentWithProject[]
}

export const deleteAssignment = async (assignmentId: number) => {
    const response =  await fetch(getUrl(`assignments/${assignmentId}`), {
        method: 'DELETE'
    });
    return await response.json();
}